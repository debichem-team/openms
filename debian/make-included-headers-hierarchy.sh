#!/bin/sh

WORK_DIR=$PWD

DEBIAN_DIR=${WORK_DIR}/debian

INCLUDES_DIR=${DEBIAN_DIR}/includes/OpenMS

rm -rf ${INCLUDES_DIR}

srcDirs="openms  openms_gui  openswathalgo superhirn"

for dir in ${srcDirs}
do
    cd src/${dir}/include/OpenMS
    
    for file in $(find . -type f | grep ".*\.h[p]\{0,2\}$" | sed 's|^./||')
    do 
        # except files in debian/ !!!
        echo ${file} | grep debian 
        if [ "$?" != "0" ]
        then
            baseName=$(basename ${file})
            # echo "baseName: ${baseName}"
            
            dirName=$(dirname ${file})
            # echo "dirName: ${dirName}"
            
            mkdir -p ${INCLUDES_DIR}/${dirName}
            cp ${file} ${INCLUDES_DIR}/${dirName}
        fi
    done

    cd ${WORK_DIR}
done
